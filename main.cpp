
#include "shared_ptr/CSharedPtr.h"
#include <cassert>

int main() {
    CSharedPtr ptr1(new int(5));
    assert(*ptr1 == 5);
    assert(ptr1.counter() == 1);

    CSharedPtr ptr2(ptr1);
    assert(*ptr2 == 5);
    assert(ptr1.counter() == 2);
    assert(ptr2.counter() == 2);

    CSharedPtr ptr3(new int(10));
    assert(*ptr3 == 10);
    assert(ptr3.counter() == 1);

    *ptr2 = 42;
    assert(*ptr1 == 42);
    assert(*ptr2 == 42);

    ptr3 = ptr1;
    assert(*ptr3 == 42);

    assert((bool) ptr3);

    CSharedPtr will_be_null;
    assert(!((bool) will_be_null));
    assert(!will_be_null);

    std::cout << *ptr1 << std::endl;
    std::cout << *ptr2 << std::endl;
    std::cout << *ptr3 << std::endl;
    std::cout << ptr1.counter() << std::endl;
    std::cout << ptr2.counter() << std::endl;

    std::cout << ptr3.counter() << std::endl;
    // TODO: Zde doplnte kod, ktery otestuje spravnost implementace
    return 0;
}
