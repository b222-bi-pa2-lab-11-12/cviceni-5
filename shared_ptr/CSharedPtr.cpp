#include "CSharedPtr.h"

CSharedPtr::CSharedPtr() {

}

CSharedPtr::CSharedPtr(T *ptr) {

}

CSharedPtr::CSharedPtr(const CSharedPtr &other) {

}

CSharedPtr::~CSharedPtr() {

}

CSharedPtr &CSharedPtr::operator=(const CSharedPtr &other) {

}

T &CSharedPtr::operator*() {

}

T *CSharedPtr::operator->() {

}

CSharedPtr::operator bool() const {

}

bool CSharedPtr::operator!() const {

}

size_t CSharedPtr::counter() const {

}
