#ifndef CVICENI_5_CSHAREDPTR_H
#define CVICENI_5_CSHAREDPTR_H
#include <iostream>

typedef int T;

class CSharedPtr {
public:
    CSharedPtr();
    CSharedPtr(T *ptr);
    CSharedPtr(const CSharedPtr &other);
    ~CSharedPtr();

    CSharedPtr &operator=(const CSharedPtr &other);

    T &operator*();
    T *operator->();
    operator bool() const;
    bool operator!() const;

    size_t counter() const;
};

#endif //CVICENI_5_CSHAREDPTR_H
