# cvičení 5

## Úvod

Na dnešním cvičení si zkusíme udlat přípravu na 4. progtest, který je jeden z nejtěžších z
důvodu, že vše si musíte napsat sami. V tomtom cvičení vám dáme možnost si některé struktůry připravit,
však řešení tohoto cvičení zveřejněno nebude

## Shared ptr

Začneme hnedka z ostra, budeme implementovat shared_ptr. Pokud víte co to je, můžete začít pracovat. Pro ty co netuší,
doporučujeme dávat pozor.

Před začátek definice třídy prosím umístěte následující kod a použijte ho, aby náš pointer byl použitelný pro různé datové typy:
```
typedef T int;
```

Naš shared ptr bude mít naimplementovaný následující funkcionality:
* konstruktor z pointeru
* kopírovací konstruktor
* detruktor
* operátor = pro kopírování
  * Zařiďte, aby fungovala varianta přijímajicí T
* operátor * - vrátí referenci T, ne který je ukazováno
* operátor -> - vrátí T pointer na objekt
* operátor bool - false, pokud je v pointeru `nullptr`, jinak true
* operátor ! - inverze k `bool`
* counter - metoda vracející `size_t` obsahující počet instancí, které ukazují na tento prvek

## Copy on write string

Jedná se o string, který po zkopírování nezkopíruje svůj obsah, ke kopii dojde teprve ve chvíly,
kdy dochází nebo hrozí jeho úprava.

Využijte ke zprovoznění již náž naimplementovaný shared_ptr.

CoW string bude mít naimplementovaný následující funkcionality:
* konstruktor ze `string`
* get(size_t N) - vrátí kopii znaku na Nté pozici
  * Neprovádí kopii, protože nedochází k úpravě stringu
* at(size_t N) - vrátí referenci na znak na Nté pozici
  * pokud je potřeba, vytvoří hlubokou kopii
* operátor << pro výpis do ostreamu
* Pokud vase reseni vyzaduje:
  * Kopírovací konstruktor
  * operátor = pro kopírování
  * operátor = ze `string`
  * destruktor
* další metody dle vašeho uvážení

## Testy

Pro toto cvičení nedodáváme žádne testy a je to z důvodu, aby jste si je zkusili napsat
sami. Dobré testy mohou vést k mnohem lepším výsledkům.

## Bonusové body

Pokud nám do konce cvičení ukážete funkční řešení obou tříd s nějakými testíky. Bude za to bodík.
